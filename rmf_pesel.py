import argparse
import urllib.request as url
from bs4 import BeautifulSoup


def check_pesel(verify):
    page = BeautifulSoup(url.urlopen("https://www.rmf.fm/r/pesel-2019.html").read(), 'html.parser')
    psl = page.find('div', attrs={'class': 'xbig'}).text.strip()

    if verify == psl:
        print("wygrałeś")
    else:
        print("przegrales")

    print("Wylosowany PESEL: ", psl)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--pesel', dest='pesel_to_verify', required=True)
    args = parser.parse_args()

    check_pesel(args.pesel_to_verify)